<?php
$plugin->version  = 2016082226;
$plugin->requires = 2010112400;  
$plugin->cron     = 0;
$plugin->release = '1.0 (Build: 2016111400)';
$plugin->maturity = MATURITY_STABLE;
$plugin->component = 'local_anziif_webservice';