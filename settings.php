<?php
defined('MOODLE_INTERNAL') || die;

if ( $hassiteconfig ){

	$settings = new admin_settingpage( 'local_anziff_webservice', 'Anziif Webservice' );

	$ADMIN->add( 'localplugins', $settings );
 
	$settings->add(new admin_setting_configtext(
		'local_anziif_webservice/assessment_duration',
		'Assessment Duration',
		'Duration day for the assessment(Number of days).',	
		1,
		PARAM_INT) 
		);

	 
}
