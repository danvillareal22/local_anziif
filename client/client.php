<?php
require_once("../../../config.php");
$token = required_param('wstoken', PARAM_TEXT);
//$token = 'daa7b2fa063c3b935d1c4fb8d01b9444';
$domainname = 'http://moodle29.ph.learnbook.com.au/';

/// FUNCTION NAME
$functionname = required_param('wsfunction', PARAM_TEXT);
//$functionname = 'local_anzif_webservice_get_course_completion';

header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
require_once('./curl.php');
$curl = new curl;

if($functionname == 'local_anzif_webservice_get_course_completion'){
	$starttime = optional_param('starttime', null, PARAM_TEXT);
	$endtime = optional_param('endtime',null, PARAM_TEXT);
	$post = xmlrpc_encode_request($functionname, array($starttime,$endtime));
}

if($functionname == 'local_anzif_webservice_course_reset'){
	$courseid = required_param('courseid',PARAM_INT);
	$userid = required_param('userid',PARAM_INT);
	$post = xmlrpc_encode_request($functionname, array($courseid,$userid));
}

//$post = xmlrpc_encode_request($functionname, array());
$resp = xmlrpc_decode($curl->post($serverurl, $post));
print_r($resp);
