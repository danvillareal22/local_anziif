<?php
$functions = array(
		'local_anziif_webservice_create_user' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'create_user',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Create user based on anziif database',
                'type'        => 'write',
        ),
		'local_anziif_webservice_update_user' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'update_user',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Update user based on anziif database',
                'type'        => 'write',
        ),
		'local_anziif_webservice_course_restore' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'course_restore',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Course restoration',
                'type'        => 'write',
        ),
		'local_anziif_webservice_send_grade' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'send_grade',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Receive Grade from CRM',
                'type'        => 'read',
        ),
		'local_anziif_webservice_receive_grade' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'receive_grade',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Receive Grade from CRM',
                'type'        => 'write',
        ),
		'local_anziif_webservice_allocate_marker' => array(
                'classname'   => 'local_anziif_webservice_external',
                'methodname'  => 'allocate_marker',
                'classpath'   => 'local/anziif_webservice/externallib.php',
                'description' => 'Allocate Marker to Assignment',
                'type'        => 'write',
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Anziif Webservice' => array(
                'functions' => array ('local_anziif_webservice_create_user',
									  'local_anziif_webservice_update_user',
									  'local_anziif_webservice_course_restore',
									  'local_anziif_webservice_send_grade',
									  'local_anziif_webservice_receive_grade',
									  'local_anziif_webservice_allocate_marker'),
                'restrictedusers' => 0,
				'shortname'=>'anziif',
                'enabled'=>1,
        )
);
