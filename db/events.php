<?php

$observers = array(
    array(
        'eventname'   => '\core\event\user_graded',
        'callback'    => '\local_anziif_webservice\observer::send_grade_event',
    )
);