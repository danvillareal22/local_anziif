<?php

require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/moodle2/backup_plan_builder.class.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot . '/backup/util/ui/import_extensions.php');
require_once($CFG->dirroot . '/course/lib.php');

//function create coursecategory
function create_new_course($categorypath,$coursefullname,$courseshortname,$startdate = null, $enddate = null){
	global $DB, $CFG;
	require_once($CFG->libdir . '/coursecatlib.php');
	mtrace("Course category: ".$data2->name." already exist");
	$course = new stdClass();
	$course->fullname = $coursefullname;
	$course->shortname = $courseshortname;
	$course->idnumber = '';
    $course->category = $categorypath;
    $course->startdate = $startdate;
	try{
		$course = create_course($course);
	}catch(Exception $e){
		mtrace($e->getMessage());
		return false;
	}
	return $course;
}

function set_assessment_date($course,$assessmentdate){
    global $DB;

    $anziifconfig = get_config('local_anziif_webservice');
    
    $modinfo = get_fast_modinfo($course);
    $section = $modinfo->get_section_info_all();
    foreach($section as $s){
        if($s->name == 'Assessment'){
            foreach ($modinfo->sections[$s->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];
                if($mod->modname == 'quiz'){
                    var_dump($mod->instance);
                    $quiz = $DB->get_record('quiz',array('id'=>$mod->instance));
                    $quiz->timeopen = $assessmentdate;
                    $quiz->timeclose = strtotime('+'.$anziifconfig->assessment_duration.' day', $quiz->timeopen);
                    $DB->update_record('quiz',$quiz);
                }
    
            }
    
        }
    }
}

//function for backup a template course
function backup_template($templateid) {
	
	$admin = get_admin();
	$config = get_config('backup');
	
	$settings = array(
		'activities' => 'backup_auto_activities',
		'blocks' => 'backup_auto_blocks',
		// 'users' => 'backup_auto_users',
		// 'role_assignments' => 'backup_auto_role_assignments',
		// 'filters' => 'backup_auto_filters',
		// 'comments' => 'backup_auto_comments',
		// 'completion_information' => 'backup_auto_userscompletion'
	);
    $bc = new backup_controller(backup::TYPE_1COURSE, $templateid, backup::FORMAT_MOODLE,
                                backup::INTERACTIVE_YES, backup::MODE_IMPORT, $admin->id);
    $backupid = $bc->get_backupid();
    $bc->get_plan()->get_setting('users')->set_status(backup_setting::LOCKED_BY_CONFIG);
    
    foreach ($settings as $setting => $configsetting) {
        if ($bc->get_plan()->setting_exists($setting)) {
            $bc->get_plan()->get_setting($setting)->set_value($config->{$configsetting});
        }
    }

    // backing up
    $bc->finish_ui();
    $bc->execute_plan();
    $bc->destroy();
    unset($bc);

    return $backupid;
}


//function to restore to course
function restore_to_course($courseid, $backupid) {
	global $CFG;
	$admin = get_admin();
	$config = get_config('backup');
    $tempdestination = $CFG->tempdir . '/backup/' . $backupid;
    if (!file_exists($tempdestination) || !is_dir($tempdestination)) {
        print_error('unknownbackupexporterror'); // shouldn't happen ever
    }

    $rc = new restore_controller($backupid, $courseid, backup::INTERACTIVE_YES, 
                                backup::MODE_IMPORT, $admin->id, 1);

    // Convert the backup if required.... it should NEVER happed
    if ($rc->get_status() == backup::STATUS_REQUIRE_CONV) {
        $rc->convert();
    }
    // Mark the UI finished.
    $rc->finish_ui();
    // Execute prechecks
    $rc->execute_precheck();
    
    if (backup::TARGET_CURRENT_DELETING == 1 || backup::TARGET_EXISTING_DELETING == 1) {
        restore_dbops::delete_course_content($courseid);
    }
    // Execute the restore.
    $rc->execute_plan();
    $rc->destroy();
    unset($rc);
    
    // Delete the temp directory now
    fulldelete($tempdestination);
}


//function send grade to ANZIIF crm api
	
function sendgrade($object,$courseidnumber,$masterid,$activityidnumber,$enrolment = null,$gradeitem){
	global $CFG;
	// $grade = var_export($object,true);
	// $myfile = fopen($CFG->dirroot ."/local/anziif_webservice/usergraded_event.txt", "w+") or die("Unable to open file!");
	// fwrite($myfile, $grade);
    // fclose($myfile);
    if($gradeitem->itemtype == 'mod'){
        mtrace('Sending activity grade');
       
         $requestParams = array(
             'assessmentResult' => array(
                 'MasterId' => $masterid,
                 'CourseId' => $courseidnumber,
                 'ActivityId' => $activityidnumber->idnumber,
                 'EnrolmentId' => $enrolment,
                 'Result' => $object->other->finalgrade,
                 'ResultDate' => date('Y-m-d\TH:i:s.u', $object->timecreated)
             ),
             'wstoken' => '3bbcee75cecc5b568031'
         );
         $client = new SoapClient('https://wst.anziif.com:8080/CYTYCWebServices/ExternalService/ExternalService.svc?wsdl');
         try{
             $apiresponse = $client->SetAssessmentResult($requestParams);
         }catch(Exception $e){
            var_dump($e->getMessage());
            if($e->getMessage() == 'CourseId must have a value'){
                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'CourseId must have a value.'
                    )
        
                ));
                $event->trigger();

                return true;
            }
            if($e->getMessage() =='ActivityId is invalid.'){

                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'Activity number:'.$activityidnumber->idnumber.' on CRM not found or invalid.'
                    )
        
                ));
                $event->trigger();

                return true;
            }
    
            if($e->getMessage() =='Invalid msaterid and mastered does not exist for user.'){

                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'Invalid msaterid: '.$masterid.' and mastered does not exist for user.'
                    )
        
                ));
                $event->trigger();
  
                return $true;
            }
    
            if($e->getMessage() =='Invalid Assessment type.'){

                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'Invalid Assessment type.'
                    )
        
                ));
                $event->trigger();
                    
                return true;
            }
    
            if($e->getMessage() =='No enrolment exist of masterid for module/unit.'){
                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'No enrolment exist of masterid for module/unit.'
                    )
        
                ));
                $event->trigger();

                return true;
            }
    
            if($e->getMessage() =='CourseId does not match with Enrolment module and studyperiod.'){

    
                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'CourseId: '.$object->courseid.' does not match with Enrolment module and studyperiod.'
                    )
        
                ));
                $event->trigger();
                
                return true;
            }
    
            if($e->getMessage() =='Invalid Result/grade.'){

                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'Invalid Result/grade.'
                    )
        
                ));
                $event->trigger();
                
                return true;
            }
    
            if($e->getMessage() =='General validation on null parameters, invalid date time with appropriate error.'){
              
                $event = \local_anziif_webservice\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => 'General validation on null parameters, invalid date time with appropriate error.'
                    )
        
                ));
                $event->trigger();
                
                return true;
            }

         }
         $event = \local_anziif_webservice\event\sending_grade::create(array(
            'objectid' => $object->objectid,
            'courseid' => $object->courseid,
            'context' => context_module::instance($activityidnumber->id),
            'relateduserid' => $object->relateduserid,
            'other' => array(
                'description' => 'Grade successfully sent to CRM!'
            )

        ));
        $event->trigger();
        mtrace('Grade successfully sent to CRM!');
        return true;
        
    }else{
        mtrace('I dont have any work to do! :)');
    }
    
}