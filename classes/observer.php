<?php
namespace local_anziif_webservice;
defined('MOODLE_INTERNAL') || die();

class observer {
    public static function send_grade_event(\core\event\user_graded $event) {
        global $CFG, $COURSE, $DB;
        $eventdata = $event->get_data();

        $gg = $DB->get_record_sql("SELECT * FROM mdl_grade_items WHERE courseid = ? AND id = ?",array($COURSE->id,$eventdata['other']['itemid'])); 
        $coursemodule = $DB->get_record('course_modules',array('instance'=>$gg->iteminstance, 'course'=>$COURSE->id));
        $user = $DB->get_record('user',array('id'=> (int) $eventdata['relateduserid']));

        $task = new \local_anziif_webservice\task\send_grade();
		$task->set_custom_data(array(
           'objectdata' => $eventdata,
           'courseidnumber' => $COURSE->idnumber,
           'masterid' => $user->idnumber,
           'activityidnumber' => $coursemodule,
           'enrolment'=>'111',
           'grade_item' =>$gg   
	    ));
        \core\task\manager::queue_adhoc_task($task);

    }
}