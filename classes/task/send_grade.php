<?php
namespace local_anziif_webservice\task;

class send_grade extends \core\task\adhoc_task
{
	public function get_component() {
        return 'local_anziif_webservice';
    }
	
    public function execute() {
        global $CFG;
		require_once($CFG->dirroot . '/local/anziif_webservice/locallib.php');
		sendgrade($this->get_custom_data()->objectdata,$this->get_custom_data()->courseidnumber,$this->get_custom_data()->masterid,$this->get_custom_data()->activityidnumber,$this->get_custom_data()->enrolment,$this->get_custom_data()->grade_item);
		
		return true;
	}
}